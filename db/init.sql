--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Debian 13.3-1.pgdg100+1)
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: POLYHOSTEL_WASH; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "POLYHOSTEL_WASH" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE "POLYHOSTEL_WASH" OWNER TO postgres;

\connect "POLYHOSTEL_WASH"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: wash_rec; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wash_rec (
    wash_rec_id integer NOT NULL,
    wash_time_id integer NOT NULL,
    washer_id integer NOT NULL,
    student_id integer NOT NULL,
    date date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE public.wash_rec OWNER TO postgres;

--
-- Name: wash_rec_wash_rec_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wash_rec_wash_rec_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wash_rec_wash_rec_id_seq OWNER TO postgres;

--
-- Name: wash_rec_wash_rec_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wash_rec_wash_rec_id_seq OWNED BY public.wash_rec.wash_rec_id;


--
-- Name: wash_times; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wash_times (
    wash_time_id integer NOT NULL,
    start_time time without time zone NOT NULL,
    end_time time without time zone NOT NULL
);


ALTER TABLE public.wash_times OWNER TO postgres;

--
-- Name: wash_times_wash_time_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wash_times_wash_time_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wash_times_wash_time_id_seq OWNER TO postgres;

--
-- Name: wash_times_wash_time_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wash_times_wash_time_id_seq OWNED BY public.wash_times.wash_time_id;


--
-- Name: washer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.washer (
    washer_id integer NOT NULL,
    hostel_id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.washer OWNER TO postgres;

--
-- Name: washer_washer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.washer_washer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.washer_washer_id_seq OWNER TO postgres;

--
-- Name: washer_washer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.washer_washer_id_seq OWNED BY public.washer.washer_id;


--
-- Name: wash_rec wash_rec_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_rec ALTER COLUMN wash_rec_id SET DEFAULT nextval('public.wash_rec_wash_rec_id_seq'::regclass);


--
-- Name: wash_times wash_time_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_times ALTER COLUMN wash_time_id SET DEFAULT nextval('public.wash_times_wash_time_id_seq'::regclass);


--
-- Name: washer washer_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.washer ALTER COLUMN washer_id SET DEFAULT nextval('public.washer_washer_id_seq'::regclass);


--
-- Data for Name: wash_rec; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: wash_times; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: washer; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: wash_rec_wash_rec_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wash_rec_wash_rec_id_seq', 1, false);


--
-- Name: wash_times_wash_time_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wash_times_wash_time_id_seq', 1, false);


--
-- Name: washer_washer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.washer_washer_id_seq', 1, false);


--
-- Name: wash_times unique_time_interval; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_times
    ADD CONSTRAINT unique_time_interval UNIQUE (start_time, end_time);


--
-- Name: wash_rec unique_wash_rec; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_rec
    ADD CONSTRAINT unique_wash_rec UNIQUE (wash_time_id, washer_id, date);


--
-- Name: wash_rec wash_rec_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_rec
    ADD CONSTRAINT wash_rec_pkey PRIMARY KEY (wash_rec_id);


--
-- Name: wash_times wash_times_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_times
    ADD CONSTRAINT wash_times_pkey PRIMARY KEY (wash_time_id);


--
-- Name: washer washer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.washer
    ADD CONSTRAINT washer_pkey PRIMARY KEY (washer_id);


--
-- Name: wash_rec wash_time_id_wash_rec; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_rec
    ADD CONSTRAINT wash_time_id_wash_rec FOREIGN KEY (wash_time_id) REFERENCES public.wash_times(wash_time_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wash_rec washer_id_wash_rec; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wash_rec
    ADD CONSTRAINT washer_id_wash_rec FOREIGN KEY (washer_id) REFERENCES public.washer(washer_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

