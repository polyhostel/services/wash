const washerServices = require('../services/washer')

const getAllWashers = async (req, res) => {
    try {
        const washers = await washerServices.asyncGetAllWashers();
        return res.json(washers)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const getWasherByName = async (req, res) => {
    try {
        const {name} = req.query;

        const washer = await washerServices.asyncGetWashersByName(name);
        return res.json(washer)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const getWasherById = async (req, res) => {
    try {
        const {id} = req.query;

        const washer = await washerServices.asyncGetWasherById(id);
        return res.json(washer)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const addWasher = async (req, res) => {
    try {
        const {hostel_id, name} = req.body;

        const washer = await washerServices.asyncAddWasher(hostel_id, name);
        return res.json(washer)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const deleteWasher = async (req, res) => {
    try {
        const {washer_id} = req.body;

        const washer = await washerServices.asyncDeleteWasher(washer_id);
        return res.json(washer)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

module.exports = {
    getAllWashers,
    getWasherByName,
    getWasherById,
    addWasher,
    deleteWasher
}