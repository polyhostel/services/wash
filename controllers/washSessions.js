const washSessionsServices = require('../services/washSessions')

const getAllSessions = async (req, res) => {
    try {
        const sessions = await washSessionsServices.asyncGetAllSessions();
        return res.json(sessions)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const getSession = async (req, res) => {
    try {
        const {session_id} = req.query;
        const session = await washSessionsServices.asyncGetSessionById(session_id);
        return res.json(session)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const getSessionByStartTime = async (req, res) => {
    try {
        const {start_time} = req.query;
        const session = await washSessionsServices.asyncGetSessionByStartTime(start_time);
        return res.json(session)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const addSession = async (req, res) => {
    try {
        const {start_time, end_time} = req.body;
        const sessions = await washSessionsServices.asyncAddSession(start_time, end_time);
        return res.json(sessions)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const deleteSession = async (req, res) => {
    try {
        const {id} = req.body;
        const status = await washSessionsServices.asyncDeleteSession(id);
        return status ? res.send('success') : res.status(404).send('Session not found')
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

module.exports = {
    getAllSessions,
    addSession,
    getSession,
    getSessionByStartTime,
    deleteSession
}