const washRecordsServices = require('../services/washRecords')
const axios = require('axios');

const getAllRecordsOnDay = async (req, res) => {
    try {
        const {date} = req.query;
        const records = await washRecordsServices.asyncGetAllRecordsOnDay(date);

        const recordsWithStudents = await Promise.all(records.map(async ({student_id, ...record}) => {
            if (student_id) {
                const student = await (axios.get(`http://user:3008/student?studentid=${student_id}`)).then((data) => data.data)
                return {...record, student}
            }
            return {...record, student: null}
        }))

        return res.json(recordsWithStudents)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const getRecordsByDate = async (req, res) => {
    try {
        const {date} = req.query;

        const records = await washRecordsServices.asyncGetRecordByDate(date);
        return res.json(records)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const getRecordById = async (req, res) => {
    try {
        const {id} = req.query;

        const record = await washRecordsServices.asyncGetRecordById(id);

        return res.json(record)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const addRecord = async (req, res) => {
    try {
        const {student_id, wash_time_id, washer_id, date} = req.body;
        const record = await washRecordsServices.asyncAddWashRec(student_id, wash_time_id, washer_id, date);
        return res.json(record)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}

const deleteRecord = async (req, res) => {
    try {
        const {id} = req.body;
        const record = await washRecordsServices.asyncDeleteWashRec(id);
        return res.json(record)
    } catch (e) {
        return res.status(400).send(String(e))
    }
}


module.exports = {
    getAllRecordsOnDay,
    addRecord,
    deleteRecord,
    getRecordsByDate,
    getRecordById
}