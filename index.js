require('dotenv').config() // переменные окружения
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser')
const sequelize = require('./db')
require('./models/init-models')
const PORT = process.env.WASH_PORT

const app = express();

const washerControllers = require('./controllers/washer')
const washSessionsControllers = require('./controllers/washSessions')
const washRecordsControllers = require('./controllers/washRecords')

app
    .use(cors())
    .use(express.json())
    .use(bodyParser.urlencoded({extended: false}))

    .get('/allwashers', washerControllers.getAllWashers)
    .get('/washerbyname', washerControllers.getWasherByName)
    .get('/washer', washerControllers.getWasherById)
    .post('/washer', washerControllers.addWasher)
    .delete('/washer', washerControllers.deleteWasher)

    .get('/allsessions', washSessionsControllers.getAllSessions)
    .get('/sessions', washSessionsControllers.getSession)
    .get('/sessionsbystarttime', washSessionsControllers.getSessionByStartTime)
    .post('/sessions', washSessionsControllers.addSession)
    .delete('/sessions', washSessionsControllers.deleteSession)

    .get('/allrecords', washRecordsControllers.getAllRecordsOnDay)
    .get('/recordsbydate', washRecordsControllers.getRecordsByDate)
    .get('/records', washRecordsControllers.getRecordById)
    .post('/records', washRecordsControllers.addRecord)
    .delete('/records', washRecordsControllers.deleteRecord)


const start = async () => {
    try {
        await sequelize.authenticate() // подключение к бд
        await sequelize.sync()
        app.listen(PORT, () => console.log(`Server started on http://localhost:${PORT}`))
    } catch (e) {
        console.log(e);
    }
}

start()

