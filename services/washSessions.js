const {WashTimes, Washer} = require('../models/init-models')

const asyncGetAllSessions = () => WashTimes.findAll(({}))

const asyncGetSessionById = (id) => WashTimes.findByPk(id).then(session => {
    if (session === null) {
        throw new Error('Session not found')
    } else {
        return session
    }
})

const asyncGetSessionByStartTime = (start_time) => WashTimes.findOne({
    attributes: ['wash_time_id'],
    where: {start_time}
}).then(session => {
    if (session === null) {
        throw new Error('Session not found')
    } else {
        return session
    }
})

const asyncAddSession = (start_time, end_time) => WashTimes.create({start_time, end_time})

const asyncDeleteSession = (id) => WashTimes.destroy({where: {wash_time_id: id}})


module.exports = {
    asyncGetAllSessions,
    asyncAddSession,
    asyncGetSessionById,
    asyncGetSessionByStartTime,
    asyncDeleteSession
}