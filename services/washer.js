const {Washer} = require('../models/init-models')
const axios = require('axios');

const asyncGetAllWashers = () => Washer.findAll()

const asyncGetWashersByName = (name) => Washer.findAll({where: {name}}).then(washer => {
    if (washer === null) {
        throw new Error('washer not found')
    } else {
        return washer
    }
})

const asyncGetWasherById = (id) => Washer.findByPk(id).then(washer => {
    if (washer === null) {
        throw new Error('washer not found')
    } else {
        return washer
    }
})

const asyncAddWasher = async (hostel_id, name) => {
    let hostel;
    try {
        hostel = (await axios.get(`http://hostel:3006/hostel?hostelid=${hostel_id}`)).data

    } catch ({response}) {
        throw new Error(response.data)
    }

    return Washer.create({hostel_id: hostel.hostel_id, name})
}

const asyncDeleteWasher = (id) => Washer.destroy({where: {washer_id: id}}).then((status) => {
    if (status === 0) {
        throw new Error('Washer not found')
    } else {
        return 'success'
    }
})


module.exports = {
    asyncGetAllWashers,
    asyncGetWashersByName,
    asyncGetWasherById,
    asyncAddWasher,
    asyncDeleteWasher
}