const {WashRec} = require('../models/init-models')
const {asyncGetSessionById} = require('../services/washSessions')
const {asyncGetWasherById} = require('../services/washer')
const sequelize = require('../db')

const axios = require('axios');

const asyncGetAllRecordsOnDay = (day = null) => sequelize.query(
    `
        select 
            wash_rec_id,
            washer.washer_id,
            wash_times.wash_time_id,
            start_time, 
            end_time, 
            name as washer_name, 
            student_id, 
            date 
            from wash_times 
        CROSS JOIN washer 
        FULL JOIN (select * from wash_rec where wash_rec.date = ${ day ? `'${day}'` : null})  as wash_rec
           ON wash_rec.wash_time_id = wash_times.wash_time_id 
           AND wash_rec.washer_id = washer.washer_id
        ORDER BY start_time
            `).then((data) => data[0])

const asyncGetRecordByDate = (date) => WashRec.findAll({
    where: {date: date}
})

const asyncGetRecordById = (id) => WashRec.findByPk(id).then((record) => {
    if (record === null) {
        throw new Error('Record not found')
    } else {
        return record
    }
})

const asyncAddWashRec = async (student_id, wash_time_id, washer_id, date) => {
    const session = await asyncGetSessionById(wash_time_id);
    if (session === null) {
        throw new Error('Session not found')
    }
    const washer = await asyncGetWasherById(washer_id);
    if (washer === null) {
        throw new Error('Washer not found')
    }

    let student;
    try {
        student = (await axios.get(`http://user:3008/student?studentid=${student_id}`)).data

    } catch ({response}) {
        throw new Error(response.data)
    }

    return WashRec.create({
        wash_time_id: session.wash_time_id,
        washer_id: washer.washer_id,
        student_id: student.student_id,
        date
    })
}

const asyncDeleteWashRec = (id) => WashRec.destroy({where: {wash_rec_id: id}}).then((status) => {
    if (status === 0) {
        throw new Error('Record not found')
    } else {
        return 'success'
    }
})


module.exports = {
    asyncGetAllRecordsOnDay,
    asyncAddWashRec,
    asyncDeleteWashRec,
    asyncGetRecordByDate,
    asyncGetRecordById
}